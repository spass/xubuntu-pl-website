var prevScrollpos = window.pageYOffset;

window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  var currentScrollPos = window.pageYOffset;
  if (prevScrollpos > currentScrollPos) {
    document.getElementById("topbar").style.top = "0";
  } else {
	  if (document.body.scrollTop > 64 || document.documentElement.scrollTop > 64) {
	    document.getElementById("topbar").style.top = "-64px";
	  } else {
    document.getElementById("topbar").style.top = "0";
	}
  }
  prevScrollpos = currentScrollPos;
}